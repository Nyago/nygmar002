#!/bin/bash

sudo python tcp.py --bw-host 1000 --bw-net 2 --delay 100 --dir ./datareno 
sudo python tcp.py --bw-host 1000 --bw-net 2 --delay 100 --dir ./datareno20 --maxq 20
sudo python tcp.py --bw-host 100 --bw-net 2 --delay 100 --dir ./datarenoh100

sudo python tcp.py --bw-host 1000 --bw-net 2 --delay 100 --dir ./datacubic --cong cubic
sudo python tcp.py --bw-host 1000 --bw-net 2 --delay 100 --dir ./datacubic20 --maxq 20 --cong cubic
sudo python tcp.py --bw-host 100 --bw-net 2 --delay 100 --dir ./datacubich100 --cong cubic

sudo python tcp.py --bw-host 1000 --bw-net 2 --delay 100 --dir ./data --cong bbr --qman fq
sudo python tcp.py --bw-host 1000 --bw-net 2 --delay 100 --dir ./data20 --maxq 20 --cong bbr --qman fq
sudo python tcp.py --bw-host 100 --bw-net 2 --delay 100 --dir ./data100 --cong bbr --qman fq

sudo python plot_ping.py -f ./datareno/ping.txt -o ./datareno/ping_reno.png
sudo python plot_ping.py -f ./datareno20/ping.txt -o ./datareno20/ping_reno20.png
sudo python plot_ping.py -f ./datarenoh100/ping.txt -o ./datarenoh100/ping_reno_100.png

sudo python plot_ping.py -f ./datacubic/ping.txt -o ./datacubic/ping_cubic.png
sudo python plot_ping.py -f ./datacubic20/ping.txt -o ./datacubic20/ping_cubic20.png
sudo python plot_ping.py -f ./datacubich100/ping.txt -o ./datacubich100/ping_cubic_100.png

sudo python plot_ping.py -f ./data/ping.txt -o ./data/ping_bbr.png
sudo python plot_ping.py -f ./data20/ping.txt -o ./data20/ping_bbr20.png
sudo python plot_ping.py -f ./data100/ping.txt -o ./data100/ping_bbr_100.png

sudo python plot_queue.py -f ./datareno/q.txt -o ./datareno/q_reno.png
sudo python plot_queue.py -f ./datareno20/q.txt -o ./datareno20/q_reno20.png
sudo python plot_queue.py -f ./datarenoh100/q.txt -o ./datarenoh100/q_reno_100.png

sudo python plot_queue.py -f ./datacubic/q.txt -o ./datacubic/q_cubic.png
sudo python plot_queue.py -f ./datacubic20/q.txt -o ./datacubic20/q_cubic20.png
sudo python plot_queue.py -f ./datacubich100/q.txt -o ./datacubich100/q_cubic_100.png

sudo python plot_queue.py -f ./data/q.txt -o ./data/q_bbr.png
sudo python plot_queue.py -f ./data20/q.txt -o ./data20/q_bbr20.png
sudo python plot_queue.py -f ./data100/q.txt -o ./data100/q_bbr_100.png
